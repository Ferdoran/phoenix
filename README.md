# Phoenix

Phoenix is a open-source server emulator for a game called Silkroad Online targetting Vietnam v188 game client.


The project is using c# as its primary programming language and .net core 2.1 (will be upgraded to ver 3 once 
the release version is out). The only DBMS currently planned to be supported is the Microsoft SQL Server (2017 for now, no 
plans upgrading any time in near future... and I doubt there will be any good reason doing so).

Official discord server: [Click here](https://discord.gg/jp7zVdp)

Twitch channel: [Click here](https://www.twitch.tv/cherno0x2f/)