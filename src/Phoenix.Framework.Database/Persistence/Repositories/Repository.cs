﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Phoenix.Framework.Database.Persistence.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly PhoenixContext context;

        public Repository(PhoenixContext context)
        {
            this.context = context;
        }

        public void Add(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return context.Set<TEntity>().ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public TEntity Get<TVal>(TVal id) where TVal : struct
        {
            return context.Set<TEntity>().Find(id);
        }

        public async Task<TEntity> GetAsync<TVal>(TVal id) where TVal : struct
        {
            return await context.Set<TEntity>().FindAsync(id);
        }

        public void Remove(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return context.Set<TEntity>().SingleOrDefault(predicate);
        }

        protected TEntity SingleOrDefault(Expression<Func<TEntity, bool>> identifierCondition, Expression<Func<TEntity, bool>> predicate)
        {
            return context.Set<TEntity>().Where(predicate).SingleOrDefault(identifierCondition);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return context.Set<TEntity>().FirstOrDefault(predicate);
        }

        protected TEntity FirstOrDefault(Expression<Func<TEntity, bool>> identifierCondition, Expression<Func<TEntity, bool>> predicate)
        {
            return context.Set<TEntity>().Where(predicate).FirstOrDefault(identifierCondition);
        }

        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> predicate)
        {
            return GetManyAsync(predicate).Result;
        }

        public async Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        protected IEnumerable<TEntity> GetMany(IQueryable<TEntity> queryableEntity, Expression<Func<TEntity, bool>> predicate)
        {
            return GetManyAsync(queryableEntity, predicate).Result.ToList();
        }

        protected async Task<IEnumerable<TEntity>> GetManyAsync(IQueryable<TEntity> queryableEntity, Expression<Func<TEntity, bool>> predicate)
        {
            return await queryableEntity.Where(predicate).ToListAsync();
        }

        public bool Exists<TVal>(TVal id) where TVal : struct
        {
            var result = Get(id);
            return (result != null) ? true : false;
        }

        public bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            var result = FirstOrDefault(predicate);
            return (result != null) ? true : false;
        }
    }
}