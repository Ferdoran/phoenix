﻿using Phoenix.Framework.Database.Models.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Persistence.Repositories
{
    class PrivilegedIPRepository : Repository<PrivilegedIP>, IPrivilegedIPRepository
    {
        public PrivilegedIPRepository(PhoenixContext context) : base(context)
        {
        }
    }
}
