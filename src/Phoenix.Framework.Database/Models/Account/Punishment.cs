﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class Punishment
    {
        public int ID { get; set; }
        public User Executor { get; set; }
        public User User { get; set; }
        [MaxLength(1024)]
        public string Reason { get; set; }
        public PunishmentType Type { get; set; }
        public DateTime TimeBegin { get; set; }
        public DateTime TimeEnd { get; set; }
    }
}
