﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Models.System
{
    public class ModuleVersion
    {
        public int ID { get; set; }
        public int Version { get; set; }
        public Module Module { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsValid { get; set; } = true;
        public DateTime RecordUpdateStamp { get; set; }

        public IEnumerable<ModuleVersionFile> ModuleVersionFiles { get; set; }
    }
}
