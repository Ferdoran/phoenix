﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Extensions
{
    public static class RandomExtensions
    {
        public static byte NextByte(this Random random) => (byte)(random.Next() & byte.MaxValue);

        public static ushort NextUShort(this Random random) => (ushort)(random.Next() & ushort.MaxValue);

        public static uint NextUInt(this Random random) => (uint)(random.Next() & uint.MaxValue);

        public static ulong NextULong(this Random random) => (ulong)(random.Next() << 32 | random.Next());
    }
}
