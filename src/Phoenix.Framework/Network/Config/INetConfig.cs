﻿namespace Phoenix.Framework.Network.Config
{
    public interface INetConfig
    {
        int SocketCount { get; }
        int TaskCount { get; }

        INetSessionFactory NetSessionFactory { get; }
    }
}