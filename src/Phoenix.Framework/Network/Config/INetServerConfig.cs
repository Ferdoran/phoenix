﻿namespace Phoenix.Framework.Network.Config
{
    public interface INetServerConfig : INetConfig
    {
        int Backlog { get; }
    }
}