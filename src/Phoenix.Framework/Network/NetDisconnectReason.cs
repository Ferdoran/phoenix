﻿namespace Phoenix.Framework.Network
{
    public enum NetDisconnectReason
    {
        NetEngine,
        Framework,
        Application,
    }
}