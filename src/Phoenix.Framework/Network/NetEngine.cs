﻿using NLog;

using Phoenix.Framework.Network.Config;
using Phoenix.Framework.Network.EventArgs;
using Phoenix.Framework.Network.Memory;
using Phoenix.Framework.Network.Messaging;
using Phoenix.Framework.Network.Messaging.Protocol;

using System;
using System.Net.Sockets;

namespace Phoenix.Framework.Network
{
    public abstract class NetEngine
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private protected readonly int _socketCount;
        private readonly MessageProcessor _messageProcessor;
        private protected readonly ISocketPool _socketPool;
        private readonly INetEventArgsPool<NetEventArgs> _receiveEventArgsPool;
        private readonly INetEventArgsPool<SendNetEventArgs> _sendEventArgsPool;
        private readonly INetEventArgsPool<DisconnectNetEventArgs> _disconnectEventArgsPool;

        public NetSessionManager SessionManager { get; }
        public NetTrafficMonitor TrafficMonitor { get; }
        public MessageManager MessageManager { get; }
        public MessageProfiler MessageProfiler { get; }

        protected NetEngine(INetConfig config)
        {
            _socketCount = config.SocketCount;
            _socketPool = new SocketPool(_socketCount);
            _socketPool.Initialize();

            _receiveEventArgsPool = new NetEventArgsPool<NetEventArgs>(_socketCount, this.ReceiveCompleted);
            _receiveEventArgsPool.Initialize();

            _sendEventArgsPool = new NetEventArgsPool<SendNetEventArgs>(_socketCount, this.SendCompleted);
            _sendEventArgsPool.Initialize();

            _disconnectEventArgsPool = new NetEventArgsPool<DisconnectNetEventArgs>(_socketCount, this.DisconnectCompleted);
            _disconnectEventArgsPool.Initialize();

            this.TrafficMonitor = new NetTrafficMonitor();

            this.MessageProfiler = new MessageProfiler();
            this.MessageManager = new MessageManager();
            this.SessionManager = new NetSessionManager(this, config.NetSessionFactory);

            _messageProcessor = new MessageProcessor(this.MessageManager, this.MessageProfiler);
            _messageProcessor.Start(config.TaskCount);

            new NetEngineHandler(this.MessageManager);
        }

        public void Send(NetSession session, Message msg)
        {
            if (!session.Encoder.TryEncode(msg))
            {
                this.Disconnect(session, NetDisconnectReason.NetEngine);
                return;
            }

            if (!_sendEventArgsPool.TryTake(out SendNetEventArgs sendArgs))
            {
                Logger.Error($"{nameof(Send)}: Failed to take {nameof(SendNetEventArgs)}!");
                this.Disconnect(session, NetDisconnectReason.NetEngine);
                return;
            }

            try
            {
                sendArgs.Session = session;
                sendArgs.Message = msg;
                sendArgs.SetBuffer(msg.Memory.Slice(0, msg.Size + Message.HeaderSize));

                // If the I/O operation is pending, the SocketAsyncEventArgs.Completed event will be raised upon completion of the operation.
                if (session.Socket.SendAsync(sendArgs))
                    return;

                // The I/O operation completed synchronously, SocketAsyncEventArgs.Completed event will not be raised.
                this.SendCompleted(session.Socket, sendArgs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                this.Disconnect(session, NetDisconnectReason.NetEngine);
                _sendEventArgsPool.TryReturn(sendArgs);
            }
        }

        private void SendCompleted(object sender, SocketAsyncEventArgs e)
        {
            var sendArgs = (SendNetEventArgs)e;
            try
            {
                if (e.SocketError != SocketError.Success || e.BytesTransferred == 0)
                {
                    Logger.Debug($"{nameof(SendCompleted)}: {e.SocketError}");
                    this.Disconnect(sendArgs.Session, NetDisconnectReason.NetEngine);
                    return;
                }

                // update stats
                this.TrafficMonitor.OnSend(e.BytesTransferred);
                sendArgs.Session.TrafficMonitor.OnSend(e.BytesTransferred);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                this.Disconnect(sendArgs.Session, NetDisconnectReason.NetEngine);
            }
            finally
            {
                _sendEventArgsPool.TryReturn(sendArgs);
            }
        }

        protected void Receive(NetSession session)
        {
            if (!_receiveEventArgsPool.TryTake(out NetEventArgs receiveArgs))
            {
                Logger.Error($"{nameof(Receive)}: Failed to take {nameof(NetEventHandler)}!");
                this.Disconnect(session, NetDisconnectReason.NetEngine);
                return;
            }

            try
            {
                receiveArgs.Session = session;
                receiveArgs.SetBuffer(session.ReceiveBuffer);

                // If the I/O operation is pending, the SocketAsyncEventArgs.Completed event will be raised upon completion of the operation.
                if (session.Socket.ReceiveAsync(receiveArgs))
                    return;

                // The I/O operation completed synchronously, SocketAsyncEventArgs.Completed event will not be raised.
                this.ReceiveCompleted(session.Socket, receiveArgs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                this.Disconnect(session, NetDisconnectReason.NetEngine);
                _receiveEventArgsPool.TryReturn(receiveArgs);
            }
        }

        private void ReceiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            var receiveArgs = (NetEventArgs)e;
            var session = receiveArgs.Session;
            try
            {
                var bytesTransferred = e.BytesTransferred;
                if (e.SocketError != SocketError.Success || bytesTransferred == 0)
                {
                    Logger.Debug($"{nameof(ReceiveCompleted)}: {e.SocketError} ({nameof(e.BytesTransferred)} = {bytesTransferred})");
                    this.Disconnect(session, NetDisconnectReason.NetEngine);
                    return;
                }

                // update stats
                this.TrafficMonitor.OnReceive(bytesTransferred);
                session.TrafficMonitor.OnReceive(bytesTransferred);
                session.KeepAliveMonitor.OnReceive();

                // decode buffer
                if (session.Decoder.TryDecode(e.MemoryBuffer.Span, bytesTransferred) != ProtocolDecodeResult.Success)
                {
                    this.Disconnect(session, NetDisconnectReason.NetEngine);
                    return;
                }

                // process decoded messages
                while (session.Decoder.TryTake(out Message msg))
                    _messageProcessor.Process(session, msg);

                this.Receive(session);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                _receiveEventArgsPool.TryReturn(receiveArgs);
            }
        }

        public void Disconnect(NetSession session, NetDisconnectReason reason)
        {
            Logger.Debug($"{nameof(Disconnect)}: {session.ID} ({reason})");
            if (!_disconnectEventArgsPool.TryTake(out DisconnectNetEventArgs disconnectArgs))
            {
                Logger.Error($"{nameof(Disconnect)}: Failed to take {nameof(DisconnectNetEventArgs)}!");
                return;
            }

            try
            {
                disconnectArgs.Session = session;
                disconnectArgs.Reason = reason;
                disconnectArgs.DisconnectReuseSocket = true;

                // If the I/O operation is pending, the SocketAsyncEventArgs.Completed event will be raised upon completion of the operation.
                if (session.Socket.DisconnectAsync(disconnectArgs))
                    return;

                // The I/O operation completed synchronously, SocketAsyncEventArgs.Completed event will not be raised.
                this.DisconnectCompleted(session.Socket, disconnectArgs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                _disconnectEventArgsPool.TryReturn(disconnectArgs);
            }
        }

        private void DisconnectCompleted(object sender, SocketAsyncEventArgs e)
        {
            var disconnectArgs = (DisconnectNetEventArgs)e;
            try
            {
                if (e.SocketError != SocketError.Success)
                {
                    Logger.Debug($"{nameof(DisconnectCompleted)}: {e.SocketError}");
                    return;
                }

                this.OnDisconnected(disconnectArgs.Session, disconnectArgs.Reason);
                _socketPool.TryReturn(disconnectArgs.Session.Socket);
            }
            finally
            {
                _disconnectEventArgsPool.TryReturn(disconnectArgs);
            }
        }

        protected virtual void OnDisconnected(NetSession session, NetDisconnectReason reason)
        {
        }
    }
}