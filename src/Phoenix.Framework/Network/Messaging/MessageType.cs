﻿namespace Phoenix.Framework.Network.Messaging
{
    public enum MessageType : byte
    {
        None = 0,
        NetEngine = 1,
        Framework = 2,
        Game = 3,
    }
}