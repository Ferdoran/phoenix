﻿using NLog;

using Phoenix.Framework.Security.Cryptography;
using Phoenix.Framework.Security.DiffieHellman;

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Phoenix.Framework.Network.Messaging.Protocol
{
    public delegate void ProtocolStateChangedEventhandler(MessageProtocolState state);

    public delegate void ProtocolCompletedEventHandler();

    internal abstract class MessageProtocol
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static readonly MessageID ReqID = MessageID.Create(MessageDirection.Req, MessageType.NetEngine, 0);
        public static readonly MessageID AckID = MessageID.Create(MessageDirection.Ack, MessageType.NetEngine, 0);

        public event ProtocolStateChangedEventhandler StateChanged;

        public event ProtocolCompletedEventHandler Completed;

        private protected NetSession _session;
        private protected MessageEncodeOption _option;

        private readonly Queue<Message> _decoded = new Queue<Message>();
        private readonly byte[] _decodeMsgSizeBuffer;
        private int _decodeMsgSizeBufferOffset;

        private Message _decodeMsg;
        private int _decodeMsgOffset = 0;
        private int _decodeMsgRemain = 0;
        protected byte[] _blowfishKey;
        private MessageProtocolState _state;

        public MessageProtocolState State
        {
            get => _state;
            protected set
            {
                Logger.Trace($"State {_state}->{value}");
                _state = value;
                this.StateChanged?.Invoke(_state);
                if (_state == MessageProtocolState.Completed)
                    this.Completed?.Invoke();
            }
        }

        internal Blowfish Blowfish { get; }
        internal MessageEncoder Encoder { get; }
        internal MessageDecoder Decoder { get; }
        internal MessageCRC CRC { get; set; }
        internal MessageSequence Sequence { get; set; }
        internal KeyExchangeBase KeyExchange { get; set; }

        public MessageProtocol(NetSession session)
        {
            _session = session;

            _decodeMsgSizeBuffer = new byte[Unsafe.SizeOf<MessageSize>()];
            _blowfishKey = new byte[sizeof(ulong)];

            this.Decoder = new MessageDecoder(this);
            this.Encoder = new MessageEncoder(this);
            this.Blowfish = new Blowfish();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal bool HasEncodingOption(MessageEncodeOption option) => (_option & option) != 0;

        internal virtual void Initialize()
        {
        }

        internal abstract MessageResult HandleReq(Message msg);

        internal abstract MessageResult HandleAck(Message msg);

        #region Decode

        internal ProtocolDecodeResult TryDecode(Span<byte> segment, int bytesTransferred)
        {
            int segmentOffset = 0;
            while (bytesTransferred > 0)
            {
                if (_decodeMsg == null)
                {
                    var sizeBytesTransferred = Math.Min(Unsafe.SizeOf<MessageSize>(), bytesTransferred);
                    segment.Slice(segmentOffset, sizeBytesTransferred).CopyTo(_decodeMsgSizeBuffer.AsSpan(_decodeMsgSizeBufferOffset));

                    _decodeMsgSizeBufferOffset += sizeBytesTransferred;
                    if (_decodeMsgSizeBufferOffset < Unsafe.SizeOf<MessageSize>())
                        break; // we need more data...

                    var msgSize = (MessageSize)(_decodeMsgSizeBuffer[1] << 8 | _decodeMsgSizeBuffer[0]);
                    if (msgSize.Encrypted && this.HasEncodingOption(MessageEncodeOption.Encryption))
                        _decodeMsgRemain = Blowfish.GetOutputLength(msgSize.DataSize + Message.EncryptSize) + Message.EncryptOffset;
                    else
                        _decodeMsgRemain = msgSize.DataSize + Message.HeaderSize;

                    if ((uint)_decodeMsgRemain > Message.BufferSize)
                        return ProtocolDecodeResult.InvalidSize;

                    _decodeMsg = MessagePool.Rent();
                    _decodeMsgOffset = 0;

                    _decodeMsgSizeBufferOffset = 0;
                }

                var dataBytesTransferred = Math.Min(_decodeMsgRemain, bytesTransferred);
                segment.Slice(segmentOffset, dataBytesTransferred).CopyTo(_decodeMsg.Memory.Span.Slice(_decodeMsgOffset));

                _decodeMsgOffset += dataBytesTransferred;
                _decodeMsgRemain -= dataBytesTransferred;
                if (_decodeMsgRemain == 0)
                {
                    if (_decodeMsg.Encrypted && this.HasEncodingOption(MessageEncodeOption.Encryption))
                        this.Blowfish.Decode(_decodeMsg.GetBufferAt(Message.EncryptOffset), _decodeMsg.GetBufferAt(Message.EncryptOffset), _decodeMsgOffset - Message.EncryptOffset);

                    //if (Logger.IsTraceEnabled)
                    //    Logger.Trace($"{nameof(TryDecode)}: {_decodeMsg.ID} [{_decodeMsg.Size} byte]\n{_decodeMsg.HexDump()}");

                    var result = this.ValidateSignature(_decodeMsg);
                    if (result != ProtocolDecodeResult.Success)
                        return result;

                    _decoded.Enqueue(_decodeMsg);
                    _decodeMsg = null;
                }

                segmentOffset += dataBytesTransferred;
                bytesTransferred -= dataBytesTransferred;
            }

            return ProtocolDecodeResult.Success;
        }

        internal abstract ProtocolDecodeResult ValidateSignature(Message msg);

        internal bool TryTakeDecoded(out Message msg) => _decoded.TryDequeue(out msg);

        #endregion Decode

        #region Encode

        internal bool TryEncode(Message msg)
        {
            //if (Logger.IsTraceEnabled)
            //    Logger.Trace($"{nameof(TryEncode)}: {msg.ID} [{msg.Size} byte]\n{msg.HexDump()}");

            if (this.HasEncodingOption(MessageEncodeOption.EDC) && msg.ID != 0x1001)
                this.Sign(msg);

            if (this.HasEncodingOption(MessageEncodeOption.Encryption) && msg.Encrypted)
            {
                var bytesToEncrypt = msg.Size + Message.EncryptSize;
                var bytesAfterEncryption = (ushort)Blowfish.GetOutputLength(bytesToEncrypt);
                if (bytesAfterEncryption + Message.EncryptOffset > Message.BufferSize)
                    return false;

                var bytesEncrypted = this.Blowfish.Encode(msg.GetBufferAt(Message.EncryptOffset), msg.GetBufferAt(Message.EncryptOffset), bytesToEncrypt);
                if (bytesEncrypted != bytesAfterEncryption)
                    return false;
            }

            return true;
        }

        internal abstract void Sign(Message msg);

        #endregion Encode
    }
}