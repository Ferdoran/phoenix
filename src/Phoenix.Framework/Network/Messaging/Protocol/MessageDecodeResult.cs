﻿namespace Phoenix.Framework.Network.Messaging.Protocol
{
    internal enum ProtocolDecodeResult
    {
        Success,
        InvalidSize,
        InvalidSequence,
        InvalidCRC,
        InvalidMessage,
        InvalidState,
    }
}