﻿using NLog;

using Phoenix.Framework.Extensions;
using Phoenix.Framework.Security.DiffieHellman;

using System;

namespace Phoenix.Framework.Network.Messaging.Protocol
{
    internal class ServerMessageProtocol : MessageProtocol
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ServerMessageProtocol(NetSession session, MessageEncodeOption option) : base(session)
        {
            _option = option;
        }

        internal override void Initialize()
        {
            using (var msgSetup = MessagePool.Rent())
            {
                msgSetup.ID = ReqID;
                msgSetup.TryWrite(_option);

                var random = new Random();

                //Encryption
                if (this.HasEncodingOption(MessageEncodeOption.Encryption))
                {
                    random.NextBytes(_blowfishKey);
                    this.Blowfish.Initialize(_blowfishKey);

                    msgSetup.TryWrite<byte>(_blowfishKey);
                }

                // EDC
                if (this.HasEncodingOption(MessageEncodeOption.EDC))
                {
                    uint sequenceSeed = random.NextByte();
                    this.Sequence = new MessageSequence(sequenceSeed);
                    msgSetup.TryWrite(sequenceSeed);

                    uint crcSeed = random.NextByte();
                    this.CRC = new MessageCRC(crcSeed);
                    msgSetup.TryWrite(crcSeed);
                }

                // Handshake
                if (this.HasEncodingOption(MessageEncodeOption.KeyExchange))
                {
                    this.KeyExchange = new KeySender();
                    this.KeyExchange.WriteSetup(msgSetup);
                }

                _session.Send(msgSetup);

                if (this.HasEncodingOption(MessageEncodeOption.KeyExchange))
                    this.State = MessageProtocolState.WaitChallenge;
                else
                    this.State = MessageProtocolState.WaitAccept;
            }
        }

        internal override MessageResult HandleReq(Message msg)
        {
            if (this.State != MessageProtocolState.WaitChallenge)
                return MessageResult.Error;

            this.KeyExchange.ReadSetup(msg);
            this.KeyExchange.ReadChallenge(msg);

            using (var msgChallenge = MessagePool.Rent())
            {
                msgChallenge.ID = ReqID;

                msgChallenge.TryWrite(MessageEncodeOption.KeyChallenge);
                this.KeyExchange.WriteChallenge(msgChallenge);

                _session.Send(msgChallenge);
            }

            this.KeyExchange.GetSecureKey(_blowfishKey);
            this.Blowfish.Initialize(_blowfishKey);

            this.State = MessageProtocolState.WaitAccept;

            return MessageResult.Success;
        }

        internal override MessageResult HandleAck(Message msg)
        {
            if (this.State != MessageProtocolState.WaitAccept)
                return MessageResult.Error;

            this.State = MessageProtocolState.Completed;
            return MessageResult.Success;
        }

        internal override ProtocolDecodeResult ValidateSignature(Message msg)
        {
            if (this.HasEncodingOption(MessageEncodeOption.EDC) && msg.ID != 0x1001)
            {
                var expectedSequence = this.Sequence.Next();
                if (msg.Sequence != expectedSequence)
                {
                    Logger.Error($"Invalid sequence on {msg.ID}");
                    return ProtocolDecodeResult.InvalidSequence;
                }

                var msgCRC = msg.CRC;
                msg.CRC = 0;

                var expectedCRC = this.CRC.Compute(msg.GetBufferAt(0, msg.Size + Message.HeaderSize));
                if (msgCRC != expectedCRC)
                {
                    Logger.Error($"Invalid CRC on {msg.ID}");
                    return ProtocolDecodeResult.InvalidCRC;
                }
            }

            return ProtocolDecodeResult.Success;
        }

        internal override void Sign(Message msg)
        {
            msg.Sequence = 0;
            msg.CRC = 0;
        }
    }
}