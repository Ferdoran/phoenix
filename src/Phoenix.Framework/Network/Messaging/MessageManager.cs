﻿using Phoenix.Framework.Network.Messaging.Protocol;

using System.Collections.Generic;

namespace Phoenix.Framework.Network.Messaging
{
    public delegate MessageResult MessageHandlerAction(NetSession session, Message msg);

    public class MessageManager
    {
        private readonly Dictionary<MessageID, MessageHandlerAction> _handlers = new Dictionary<MessageID, MessageHandlerAction>();

        public void RegisterHandler(MessageID msgID, MessageHandlerAction action)
        {
            _handlers[msgID] = action;
        }

        internal MessageResult Handle(NetSession session, Message msg)
        {
            // Make sure
            if (session.Protocol.State != MessageProtocolState.Completed && msg.ID.Type != MessageType.NetEngine)
                return MessageResult.Error;

            if (_handlers.TryGetValue(msg.ID, out MessageHandlerAction action))
                return action.Invoke(session, msg);

            return MessageResult.Unhandled;
        }
    }
}