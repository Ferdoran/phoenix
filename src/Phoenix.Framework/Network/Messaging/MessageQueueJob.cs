﻿namespace Phoenix.Framework.Network.Messaging
{
    internal class MessageQueueJob
    {
        public NetSession Session { get; set; }
        public Message Message { get; set; }

        public MessageQueueJob(NetSession session, Message message)
        {
            this.Session = session;
            this.Message = message;
        }
    }
}