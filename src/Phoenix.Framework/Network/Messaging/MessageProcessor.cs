﻿using NLog;

using System.Collections.Generic;
using System.Threading;

namespace Phoenix.Framework.Network.Messaging
{
    internal class MessageProcessor
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly CancellationTokenSource _cancellationTokenSource;

        private readonly MessageManager _manager;
        private readonly MessageProfiler _profiler;

        private int _queueCount;
        private List<MessageQueue> _queues;

        public MessageProcessor(MessageManager manager, MessageProfiler profiler)
        {
            _manager = manager;
            _profiler = profiler;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public void Start(int queueCount)
        {
            _queueCount = queueCount;
            _queues = new List<MessageQueue>(queueCount);
            for (int i = 0; i < _queueCount; i++)
                _queues.Add(new MessageQueue(_manager, _profiler, _cancellationTokenSource.Token));
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
            for (int i = 0; i < _queueCount; i++)
            {

            }
        }

        public void Process(NetSession session, Message message)
        {
            // TODO: Better load balancing on the queues.
            _queues[session.ID % _queueCount].Process(session, message);
        }
    }
}