﻿using System;
using System.Buffers;
using System.Collections.Concurrent;

namespace Phoenix.Framework.Network.Messaging
{
    public static class MessagePool
    {
        private static readonly MessageBufferPool _bufferPool = new MessageBufferPool();

        public static Message Rent() => new Message(_bufferPool.Rent());

        private class MessageBufferPool : MemoryPool<byte>
        {
            private class MessageBufferOwner : IMemoryOwner<byte>
            {
                private readonly MessageBufferPool _pool;
                private byte[] _buffer;

                public MessageBufferOwner(MessageBufferPool pool, byte[] buffer)
                {
                    _pool = pool;
                    _buffer = buffer;
                }

                public Memory<byte> Memory
                {
                    get
                    {
                        var buffer = _buffer; //use a copy of the reference
                        if (buffer == null)
                            throw new ObjectDisposedException(nameof(buffer));

                        return buffer.AsMemory();
                    }
                }

                public void Dispose()
                {
                    var buffer = _buffer;
                    if (buffer == null)
                        return;

                    _buffer = null;
                    _pool.Return(this);
                }
            }

            public override int MaxBufferSize => Message.BufferSize;

            private readonly ConcurrentDictionary<MessageBufferOwner, byte[]> _nodes;
            private readonly ArrayPool<byte> _arrayPool;

            public MessageBufferPool()
            {
                _nodes = new ConcurrentDictionary<MessageBufferOwner, byte[]>();
                _arrayPool = ArrayPool<byte>.Create(4096, 1024 * Environment.ProcessorCount);
            }

            public override IMemoryOwner<byte> Rent(int minBufferSize = -1)
            {
                var buffer = _arrayPool.Rent(Message.BufferSize);
                var node = new MessageBufferOwner(this, buffer);
                _nodes[node] = buffer;
                return node;
            }

            private void Return(MessageBufferOwner node)
            {
                if (_nodes.TryRemove(node, out byte[] value))
                    _arrayPool.Return(value, clearArray: true);
            }

            protected override void Dispose(bool disposing)
            {
                foreach (var node in _nodes.Keys)
                    node.Dispose();
            }
        }
    }
}