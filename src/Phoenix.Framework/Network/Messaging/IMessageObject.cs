﻿namespace Phoenix.Framework.Network.Messaging
{
    internal interface IMessageObject
    {
        MessageResult Read(MessageReader reader);

        MessageResult Write(MessageWriter writer);
    }
}