﻿using System;
using System.Buffers;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace Phoenix.Framework.Network.Messaging
{
    public class Message : IMessage
    {
        #region Const

        public const ushort BufferSize = 4096;
        public const ushort HeaderSize = 6;
        public const ushort DataSize = BufferSize - HeaderSize;

        public const ushort HeaderOffset = 0;
        public const ushort SizeOffset = HeaderOffset + 0;
        public const ushort IDOffset = HeaderOffset + 2;
        public const ushort SequenceOffset = HeaderOffset + 4;
        public const ushort CRCOffset = HeaderOffset + 5;
        public const ushort DataOffset = HeaderOffset + HeaderSize;

        public const ushort EncryptOffset = HeaderOffset + 2;
        public const ushort EncryptSize = HeaderSize - HeaderOffset;
        public const ushort EncryptMask = 0x8000;

        #endregion Const

        #region Fields

        private readonly IMemoryOwner<byte> _owner;
        private readonly Memory<byte> _memory;
        private readonly Memory<byte> _dataMemory;

        private int _readPosition = 0;
        private int _writePosition = 0;

        #endregion Fields

        #region Properties

        public ushort Size
        {
            get
            {
                return (ushort)(MemoryMarshal.Read<ushort>(_memory.Span.Slice(SizeOffset, Unsafe.SizeOf<ushort>())) & ~EncryptMask);
            }
            set
            {
                var span = _memory.Span.Slice(SizeOffset, Unsafe.SizeOf<ushort>());
                var size = (ushort)(value | MemoryMarshal.Read<ushort>(span) & EncryptMask);
                MemoryMarshal.Write(_memory.Span.Slice(SizeOffset, Unsafe.SizeOf<ushort>()), ref size);
            }
        }

        public bool Encrypted
        {
            get => (MemoryMarshal.Read<ushort>(_memory.Span.Slice(SizeOffset, Unsafe.SizeOf<ushort>())) & EncryptMask) != 0;
            set
            {
                var span = _memory.Span.Slice(SizeOffset, Unsafe.SizeOf<ushort>());
                var size = (ushort)(MemoryMarshal.Read<ushort>(span) & ~EncryptMask | (value ? EncryptMask : default));
                MemoryMarshal.Write(_memory.Span.Slice(SizeOffset, Unsafe.SizeOf<ushort>()), ref size);
            }
        }

        public MessageID ID
        {
            get => MemoryMarshal.Read<MessageID>(_memory.Span.Slice(IDOffset, Unsafe.SizeOf<MessageID>()));
            set => MemoryMarshal.Write(_memory.Span.Slice(IDOffset, Unsafe.SizeOf<MessageID>()), ref value);
        }

        internal byte Sequence
        {
            get => _memory.Span[SequenceOffset];
            set => _memory.Span[SequenceOffset] = value;
        }

        internal byte CRC
        {
            get => _memory.Span[CRCOffset];
            set => _memory.Span[CRCOffset] = value;
        }

        internal Memory<byte> Memory => _memory;

        #endregion Properties

        internal Message(IMemoryOwner<byte> owner)
        {
            _owner = owner;
            _memory = owner.Memory;
            _dataMemory = _memory.Slice(DataOffset, DataSize);
        }

        internal void CopyTo(Message message) => _memory.Span.Slice(0, HeaderSize + this.Size).CopyTo(message._memory.Span);

        public Message Clone()
        {
            var clone = MessagePool.Rent();
            this.CopyTo(clone);
            return clone;
        }

        object ICloneable.Clone() => this.Clone();

        internal Span<byte> GetBufferAt(int start) => _memory.Span.Slice(start);

        internal Span<byte> GetBufferAt(int start, int length) => _memory.Span.Slice(start, length);

        public bool TryRead<T>(out T value)
           where T : unmanaged
        {
            ushort size = (ushort)Unsafe.SizeOf<T>();
            if (_readPosition + size > this.Size)
            {
                value = default;
                return false;
            }

            if (MemoryMarshal.TryRead(_dataMemory.Span.Slice(_readPosition), out value))
            {
                _readPosition += size;
                return true;
            }
            return false;
        }

        public bool TryRead<T>(in Span<T> values) where T : unmanaged
        {
            var valueBytes = MemoryMarshal.AsBytes(values);

            ushort size = (ushort)valueBytes.Length;
            if (_readPosition + size > this.Size)
                return false;

            if (_dataMemory.Span.Slice(_readPosition, size).TryCopyTo(valueBytes))
            {
                _readPosition += size;
                return true;
            }
            return false;
        }

        public bool TryRead(out string value) => this.TryRead(out value, Encoding.ASCII);

        public bool TryRead(out string value, Encoding encoding)
        {
            if (!this.TryRead(out ushort length))
            {
                value = null;
                return false;
            }

            return this.TryRead(out value, length, encoding);
        }

        public bool TryRead(out string value, ushort length) => this.TryRead(out value, length, Encoding.ASCII);

        public bool TryRead(out string value, ushort length, Encoding encoding)
        {
            if (_readPosition + length > this.Size)
            {
                value = null;
                return false;
            }

            value = encoding.GetString(_dataMemory.Span.Slice(_readPosition, length));
            return true;
        }

        public bool TryWrite<T>(T value) where T : unmanaged => this.TryWrite(ref value);

        public bool TryWrite<T>(ref T value)
            where T : unmanaged
        {
            if (MemoryMarshal.TryWrite(_dataMemory.Span.Slice(_writePosition), ref value))
            {
                _writePosition += (ushort)Unsafe.SizeOf<T>();
                this.Size = (ushort)_writePosition;
                return true;
            }
            return false;
        }

        public bool TryWrite(string value) => this.TryWrite(value, Encoding.ASCII);

        public bool TryWrite(string value, Encoding encoding)
        {
            var byteCount = (ushort)encoding.GetByteCount(value);
            if (_writePosition + sizeof(ushort) + byteCount > BufferSize)
                return false;

            if (!this.TryWrite(byteCount))
                return false;

            var result = encoding.GetBytes(value, _dataMemory.Span.Slice(_writePosition));
            if (result > 0)
            {
                _writePosition += byteCount;
                this.Size = (ushort)_writePosition;
                return true;
            }

            return false;
        }

        public bool TryWrite<T>(in ReadOnlySpan<T> values)
            where T : unmanaged
        {
            var valueBytes = MemoryMarshal.AsBytes(values);
            if (valueBytes.TryCopyTo(_dataMemory.Span.Slice(_writePosition)))
            {
                _writePosition += (ushort)valueBytes.Length;
                this.Size = (ushort)_writePosition;
                return true;
            }
            return false;
        }

        #region IDisposeable

        public bool Disposed { get; private set; }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                    _owner.Dispose();
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.

                this.Disposed = true;
            }
        }

        // Use C# destructor syntax for finalization code.
        ~Message()
        {
            Debug.Fail($"{nameof(Message)} {this.ID} was collected by GC. Make sure you properly dispose!");
            this.Dispose(false);
        }

        #endregion IDisposeable
    }
}