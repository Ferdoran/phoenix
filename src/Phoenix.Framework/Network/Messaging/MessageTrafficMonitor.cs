﻿using System.Threading;

namespace Phoenix.Framework.Network.Messaging
{
    public class MessageTrafficMonitor : NetTrafficMonitor
    {
        private int _handled;
        private long _ticks;

        public void Handled(long elapsed)
        {
            Interlocked.Increment(ref _handled);
            Interlocked.Add(ref _ticks, elapsed);
        }

        public override void Reset()
        {
            base.Reset();

            Interlocked.Exchange(ref _handled, 0);
            Interlocked.Exchange(ref _ticks, 0);
        }
    }
}