﻿using System;
using System.Runtime.InteropServices;

namespace Phoenix.Framework.Network.Messaging
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = sizeof(ushort))]
    public struct MessageSize : IEquatable<MessageSize>
    {
        #region Reasons to use C++

        // (MSB)                                                                      (LSB)
        // | 15 | 14 | 13 | 12 | 11 | 10 | 09 | 08 | 07 | 06 | 05 | 04 | 03 | 02 | 01 | 00 |
        // | E* |                                   SIZE                                   |
        //
        // E* = EncryptionFlag

        private const int SIZE_SIZE = 15;
        private const int SIZE_OFFSET = 0;
        private const ushort SIZE_MASK = ((1 << SIZE_SIZE) - 1) << SIZE_OFFSET;

        private const int ENCRYPTED_SIZE = 1;
        private const int ENCRYPTED_OFFSET = SIZE_OFFSET + SIZE_SIZE;
        private const ushort ENCRYPTED_MASK = ((1 << ENCRYPTED_SIZE) - 1) << ENCRYPTED_OFFSET;

        #endregion Reasons to use C++

        #region Properties

        public ushort Value { get; set; }

        public ushort DataSize
        {
            get => (ushort)((this.Value & SIZE_MASK) >> SIZE_OFFSET);
            set => this.Value = (ushort)((this.Value & ~SIZE_MASK) | ((value << SIZE_OFFSET) & SIZE_MASK));
        }

        public bool Encrypted
        {
            get => Convert.ToBoolean((this.Value & ENCRYPTED_MASK) >> ENCRYPTED_OFFSET);
            set => this.Value = (ushort)((this.Value & ~ENCRYPTED_MASK) | ((Convert.ToByte(value) << ENCRYPTED_OFFSET) & ENCRYPTED_MASK));
        }

        #endregion Properties

        public MessageSize(ushort value) => this.Value = value;

        public MessageSize(ushort size, bool encrypted)
        {
            this.Value = (ushort)(size | (encrypted ? Message.EncryptMask : default));
        }

        public override string ToString()
        {
            if (this.Encrypted)
                return $"{this.DataSize} (Encrypted)";

            return this.DataSize.ToString();
        }

        #region IEquatable

        public override bool Equals(object obj) => obj is MessageSize size && this.Equals(size);

        public bool Equals(MessageSize other) => this.Value == other.Value;

        public override int GetHashCode() => HashCode.Combine(this.Value);

        public static bool operator ==(MessageSize left, MessageSize right) => left.Equals(right);

        public static bool operator !=(MessageSize left, MessageSize right) => !(left == right);

        #endregion IEquatable

        public static implicit operator ushort(MessageSize size) => size.DataSize;

        public static explicit operator MessageSize(ushort size) => new MessageSize(size);
    }
}