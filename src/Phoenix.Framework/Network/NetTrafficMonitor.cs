﻿using System.Threading;

namespace Phoenix.Framework.Network
{
    public class NetTrafficMonitor
    {
        private int _receivedSegments;
        private long _receivedBytes;
        private int _sentSegments;
        private long _sentBytes;

        public int ReceivedSegments => Interlocked.CompareExchange(ref _receivedSegments, 0, 0);
        public long ReceivedBytes => Interlocked.CompareExchange(ref _receivedBytes, 0L, 0L);

        public int SentSegments => Interlocked.CompareExchange(ref _sentSegments, 0, 0);
        public long SentBytes => Interlocked.CompareExchange(ref _sentBytes, 0L, 0L);

        public void OnReceive(int count)
        {
            Interlocked.Increment(ref _receivedSegments);
            Interlocked.Add(ref _receivedBytes, count);
        }

        public void OnSend(int count)
        {
            Interlocked.Increment(ref _sentSegments);
            Interlocked.Add(ref _sentBytes, count);
        }

        public virtual void Reset()
        {
            Interlocked.Exchange(ref _receivedSegments, 0);
            Interlocked.Exchange(ref _receivedBytes, 0);

            Interlocked.Exchange(ref _sentSegments, 0);
            Interlocked.Exchange(ref _sentBytes, 0);
        }
    }
}