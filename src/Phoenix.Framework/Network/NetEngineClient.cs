﻿using NLog;

using Phoenix.Framework.Network.Config;
using Phoenix.Framework.Network.EventArgs;

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network
{
    public abstract class NetEngineClient : NetEngine
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly bool _keepAlive;
        private readonly int _connectionTimeout;
        private readonly INetEventArgsPool<ConnectNetEventArgs> _connectEventArgsPool;

        public NetEngineClient(INetClientConfig config) : base(config)
        {
            _keepAlive = config.KeepAlive;
            _connectionTimeout = config.ConnectionTimeout;

            _connectEventArgsPool = new NetEventArgsPool<ConnectNetEventArgs>(_socketCount, this.ConnectCompleted);
            _connectEventArgsPool.Initialize();
        }

        public NetSession Connect(string hostOrIP, ushort port) => this.Connect(NetHelper.ToEndPoint(hostOrIP, port));

        public NetSession Connect(EndPoint remoteEndPoint)
        {
            var session = this.SessionManager.CreateSession();
            this.ConnectInternal(session, remoteEndPoint);
            return session;
        }

        internal bool ConnectInternal(NetSession session, EndPoint remoteEndPoint)
        {
            if (!_socketPool.TryTake(out Socket socket, 1000))
            {
                Logger.Error($"{nameof(ConnectInternal)}: Failed to take {nameof(Socket)}!");
                return false;
            }
            Debug.Assert(socket != null);
            session.Socket = socket;

            // Take an EventAgrs from pool.
            if (!_connectEventArgsPool.TryTake(out ConnectNetEventArgs connectArgs, 1000))
            {
                Logger.Error($"{nameof(ConnectInternal)}: Failed to take {nameof(SocketAsyncEventArgs)}!");
                _socketPool.TryReturn(in socket);
                return false;
            }

            try
            {
                Logger.Info($"Connecting to {remoteEndPoint}");

                connectArgs.Session = session;
                connectArgs.RemoteEndPoint = remoteEndPoint;

                // If the I/O operation is pending, the SocketAsyncEventArgs.Completed event will be raised upon completion of the operation.
                //if (Socket.ConnectAsync(SocketType.Stream, ProtocolType.Tcp, e))
                if (socket.ConnectAsync(connectArgs))
                {
                    connectArgs.CancelAfter(_connectionTimeout);
                    return true;
                }

                // The I/O operation completed synchronously, SocketAsyncEventArgs.Completed event will not be raised.
                this.ConnectCompleted(session.Socket, connectArgs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                _connectEventArgsPool.TryReturn(connectArgs);
                _socketPool.TryReturn(socket);
                return false;
            }

            return true;
        }

        private void ConnectCompleted(object sender, SocketAsyncEventArgs e)
        {
            var connectArgs = (ConnectNetEventArgs)e;
            var session = connectArgs.Session;
            try
            {
                if (e.SocketError != SocketError.Success)
                {
                    Logger.Debug($"{nameof(ConnectCompleted)}: {e.SocketError}");
                    if (_keepAlive)
                    {
                        var remoteEndPoint = e.RemoteEndPoint;
                        Logger.Warn($"Cannot establish keep alive session with {NetHelper.AddressFromEndPoint(remoteEndPoint)}");
                        Task.Run(() => this.ConnectInternal(session, remoteEndPoint));
                    }
                    _socketPool.TryReturn(session.Socket);
                    return;
                }

                session.Socket = e.ConnectSocket;

                this.OnConnected(session);
                this.Receive(session);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                _connectEventArgsPool.TryReturn(connectArgs);
            }
        }

        protected virtual void OnConnected(NetSession session)
        {
            session.Initialize();
        }
    }
}