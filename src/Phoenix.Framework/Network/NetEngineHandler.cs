﻿using Phoenix.Framework.Network.Messaging;
using Phoenix.Framework.Network.Messaging.Protocol;

namespace Phoenix.Framework.Network
{
    internal class NetEngineHandler : MessageHandler
    {
        public NetEngineHandler(MessageManager manager) : base(manager)
        {
            manager.RegisterHandler(MessageProtocol.ReqID, this.OnNetEngineReq);
            manager.RegisterHandler(MessageProtocol.AckID, this.OnNetEngineAck);
        }

        private MessageResult OnNetEngineReq(NetSession session, Message msg) => session.Protocol.HandleReq(msg);

        private MessageResult OnNetEngineAck(NetSession session, Message msg) => session.Protocol.HandleAck(msg);
    }
}