﻿using System.Net.Sockets;

namespace Phoenix.Framework.Network.EventArgs
{
    internal class NetEventArgs : SocketAsyncEventArgs
    {
        public NetSession Session { get; set; }
    }
}