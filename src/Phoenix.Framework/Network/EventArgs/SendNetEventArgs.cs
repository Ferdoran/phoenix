﻿using Phoenix.Framework.Network.Messaging;

using System.Net.Sockets;

namespace Phoenix.Framework.Network.EventArgs
{
    internal class SendNetEventArgs : NetEventArgs
    {
        public Message Message { get; set; }

        protected override void OnCompleted(SocketAsyncEventArgs e)
        {
            this.Message.Dispose();
            base.OnCompleted(e);
        }
    }
}