﻿namespace Phoenix.Framework.Network.EventArgs
{
    internal class DisconnectNetEventArgs : NetEventArgs
    {
        public NetDisconnectReason Reason { get; set; }
    }
}