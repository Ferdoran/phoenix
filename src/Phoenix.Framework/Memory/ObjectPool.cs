﻿using NLog;

using System.Collections.Concurrent;
using System.Threading;

namespace Phoenix.Framework.Memory
{
    public abstract class ObjectPool<T> : IObjectPool<T>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly BlockingCollection<T> _collection;
        private readonly int _size;
        private readonly bool _canAllocate;

        protected ObjectPool(int size = -1, bool canAllocate = false)
        {
            _size = size;
            _canAllocate = canAllocate;
            _collection = new BlockingCollection<T>(size);
        }

        protected abstract T Create();

        protected virtual void Clean(in T obj)
        {
        }

        protected abstract void Destroy(in T obj);

        public void Initialize()
        {
            for (int i = 0; i < _size; i++)
            {
                var obj = this.Create();
                if (obj == null)
                    return;

                this.TryReturn(obj);
            }
        }

        public bool TryTake(out T obj, int timeout = Timeout.Infinite)
        {
            var result = _collection.TryTake(out obj, timeout);
            if (!result && _canAllocate)
            {
                obj = this.Create();
                if (obj == null)
                    return false;

                if (Logger.IsTraceEnabled)
                    Logger.Trace($"{this}::{nameof(TryTake)}: Object allocated!");

                return true;
            }
            return result;
        }

        public bool TryReturn(in T obj)
        {
            this.Clean(in obj);

            if (!_collection.TryAdd(obj))
            {
                this.Destroy(in obj);
                if (Logger.IsTraceEnabled)
                    Logger.Trace($"{this}::{nameof(TryReturn)}: Object destroyed!");

                return false;
            }
            return true;
        }
    }
}