﻿namespace Phoenix.Framework.Memory
{
    public interface IObjectPool<T>
    {
        void Initialize();

        bool TryReturn(in T obj);

        bool TryTake(out T obj, int timeout = -1);
    }
}