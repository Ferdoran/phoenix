﻿using NLog;

using Phoenix.Framework.Network.Messaging;

using System;

namespace Phoenix.Framework.Security.DiffieHellman
{
    internal class KeyRecipent : KeyExchangeBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override void WriteSetup(Message msg)
        {
            msg.TryWrite(_localPublic);
        }

        internal override void ReadSetup(Message msg)
        {
            if (!msg.TryRead<byte>(_key))
                throw new KeyExchangeException("Failed to read key.");

            if (Logger.IsTraceEnabled)
                Logger.Trace($"Key: {BitConverter.ToString(_key)}");

            if (!msg.TryRead(out uint generator))
                throw new KeyExchangeException("Failed to read generator.");

            if (!msg.TryRead(out uint prime))
                throw new KeyExchangeException("Failed to read prime.");

            if (!msg.TryRead(out uint remotePublic))
                throw new KeyExchangeException("Failed to read public.");

            this.SetupLocal(generator, prime);
            this.SetupRemote(remotePublic);
        }

        internal override void ReadChallenge(Message msg)
        {
            if (!msg.TryRead<byte>(_remoteChallenge))
                throw new KeyExchangeException("Failed to read challenge.");

            if (Logger.IsTraceEnabled)
                Logger.Trace($"RemoteChallenge: {BitConverter.ToString(_remoteChallenge)}");

            for (int i = 0; i < sizeof(ulong); i++)
            {
                if (_localChallenge[i] != _remoteChallenge[i])
                    throw new KeyExchangeException("Invalid signature.");
            }
            _hasCompleted = true;
        }
    }
}