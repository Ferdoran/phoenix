﻿
using System;

namespace Phoenix.Framework.Security.DiffieHellman
{
    [Serializable]
    public class KeyExchangeException : Exception
    {
        public KeyExchangeException()
        {
        }

        public KeyExchangeException(string message) : base(message)
        {
        }

        public KeyExchangeException(string message, Exception inner) : base(message, inner)
        {
        }

        protected KeyExchangeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}