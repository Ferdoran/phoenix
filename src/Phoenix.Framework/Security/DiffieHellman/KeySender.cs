﻿using NLog;

using Phoenix.Framework.Extensions;
using Phoenix.Framework.Network.Messaging;

using System;

namespace Phoenix.Framework.Security.DiffieHellman
{
    internal class KeySender : KeyExchangeBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public KeySender()
        {
            var generator = _random.NextUInt() & int.MaxValue;
            var prime = _random.NextUInt() & int.MaxValue;
            // TODO: Avoid 0 values, leads to client crash

            _random.NextBytes(_key);
            if (Logger.IsTraceEnabled)
                Logger.Trace($"Key: {BitConverter.ToString(_key)}");

            this.SetupLocal(generator, prime);
        }

        public override void WriteSetup(Message msg)
        {
            msg.TryWrite<byte>(_key);
            msg.TryWrite(_generator);
            msg.TryWrite(_prime);
            msg.TryWrite(_localPublic);
        }

        internal override void ReadSetup(Message msg)
        {
            if (!msg.TryRead(out uint remotePublic))
                throw new KeyExchangeException("Failed to read public.");

            this.SetupRemote(remotePublic);
        }

        internal override void ReadChallenge(Message msg)
        {
            if (!msg.TryRead<byte>(_remoteChallenge))
                throw new KeyExchangeException("Failed to read challenge.");

            if (Logger.IsTraceEnabled)
                Logger.Trace($"RemoteChallenge: {BitConverter.ToString(_remoteChallenge)}");

            for (int i = 0; i < sizeof(ulong); i++)
            {
                if (_localChallenge[i] != _remoteChallenge[i])
                    throw new KeyExchangeException("Invalid signature.");
            }
            _hasCompleted = true;
        }
    }
}