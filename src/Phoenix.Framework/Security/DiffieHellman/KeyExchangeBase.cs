﻿using NLog;

using Phoenix.Framework.Extensions;
using Phoenix.Framework.Network.Messaging;
using Phoenix.Framework.Security.Cryptography;

using System;

namespace Phoenix.Framework.Security.DiffieHellman
{
    internal abstract class KeyExchangeBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private protected readonly Random _random;
        private readonly Blowfish _blowfish;

        private protected uint _prime;
        private protected uint _generator;
        private protected uint _private;
        private protected uint _localPublic;
        private protected uint _remotePublic;
        private protected uint _commonSecret;

        private protected readonly byte[] _key;

        private readonly byte[] _blowfishKey;
        private protected readonly byte[] _localChallenge;
        private protected readonly byte[] _remoteChallenge;

        private bool _hasLocalSetup;
        private bool _hasRemoteSetup;
        private protected bool _hasCompleted;

        public void GetSecureKey(Span<byte> key)
        {
            if (!_hasCompleted)
                throw new KeyExchangeException("Key exchange has not yet been completed.");

            _key.CopyTo(key);
            KeyExchangeHelper.KeyTransformValue(key, _commonSecret, (byte)(_commonSecret & 0x03));

            if (Logger.IsTraceEnabled)
                Logger.Trace($"SecureKey: {BitConverter.ToString(key.ToArray())}");
        }

        public KeyExchangeBase()
        {
            _key = new byte[sizeof(ulong)];

            _blowfishKey = new byte[sizeof(ulong)];
            _localChallenge = new byte[sizeof(ulong)];
            _remoteChallenge = new byte[sizeof(ulong)];

            _random = new Random();
            _blowfish = new Blowfish();

            _private = _random.NextUInt() & int.MaxValue;
            if (Logger.IsTraceEnabled)
                Logger.Trace($"Private: {_private}");
        }

        private protected void SetupLocal(uint generator, uint prime)
        {
            if (_hasLocalSetup)
                throw new KeyExchangeException("Local setup has already been completed.");

            _generator = generator;
            _prime = prime;
            _localPublic = KeyExchangeHelper.G_pow_X_mod_P(_generator, _private, _prime);
            _hasLocalSetup = true;

            if (Logger.IsTraceEnabled)
            {
                Logger.Trace($"Generator: {_generator}");
                Logger.Trace($"Prime: {_prime}");
                Logger.Trace($"LocalPublic: {_localPublic}");
            }
        }

        private protected void SetupRemote(uint remotePublic)
        {
            if (_hasRemoteSetup)
                throw new KeyExchangeException("Remote setup has already been completed.");

            if (!_hasLocalSetup)
                throw new KeyExchangeException("Local setup has not yet been completed.");

            _remotePublic = remotePublic;

            _commonSecret = KeyExchangeHelper.G_pow_X_mod_P(_remotePublic, _private, _prime);

            KeyExchangeHelper.KeyTransformValue(_blowfishKey, _commonSecret, (byte)(_commonSecret & 0x03));
            _blowfish.Initialize(_blowfishKey);

            KeyExchangeHelper.KeyTransformValue(_localChallenge, _commonSecret, (byte)(_commonSecret & 0x07));
            _blowfish.Encode(_localChallenge, _localChallenge);

            _hasRemoteSetup = true;

            if (Logger.IsTraceEnabled)
            {
                Logger.Trace($"RemotePublic: {_remotePublic}");
                Logger.Trace($"CommonSecret: {_commonSecret}");
                Logger.Trace($"BlowfishKey: {BitConverter.ToString(_blowfishKey)}");
                Logger.Trace($"LocalChallange: {BitConverter.ToString(_localChallenge)}");
            }
        }

        public abstract void WriteSetup(Message msg);

        internal abstract void ReadSetup(Message msg);

        internal virtual void WriteChallenge(Message msgAck)
        {
            msgAck.TryWrite<byte>(_localChallenge);
        }

        internal abstract void ReadChallenge(Message msg);
    }
}