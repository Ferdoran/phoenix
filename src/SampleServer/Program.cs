﻿using NLog;

using Phoenix.Framework.Network;
using Phoenix.Framework.Network.Config;
using Phoenix.Framework.Network.Messaging;

using System;
using System.Threading;

namespace SampleServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "SampleServer";
            var service = new SampleServerService();
            service.Run();
            Console.ReadLine();
        }
    }

    internal class SampleServerService
    {
        public SampleServerNet Network { get; set; }

        public string Name { get; set; }

        public SampleServerService()
        {
            this.Name = "SampleServerService";
            this.Network = new SampleServerNet(this, new SampleServerNetworkConfig());
        }

        public void Run()
        {
            this.Network.Start("127.0.0.1", 16000);

            int lastReceivedSegments = 0;
            long lastReceivedBytes = 0;
            int lastSentSegments = 0;
            long lastSentBytes = 0;
            while (true)
            {
                int currentReceivedSegments = this.Network.TrafficMonitor.ReceivedSegments;
                long currentReceivedBytes = this.Network.TrafficMonitor.ReceivedBytes;
                int currentSentSegments = this.Network.TrafficMonitor.SentSegments;
                long currentSentBytes = this.Network.TrafficMonitor.SentBytes;

                Console.WriteLine($"Received segments/s: {currentReceivedSegments - lastReceivedSegments}");
                Console.WriteLine($"Received bytes/s: {currentReceivedBytes - lastReceivedBytes}");
                Console.WriteLine($"Sent segments/s: {currentSentSegments - lastSentSegments}");
                Console.WriteLine($"Sent bytes/s: {currentSentBytes - lastSentBytes}");
                Console.WriteLine();

                lastReceivedSegments = currentReceivedSegments;
                lastReceivedBytes = currentReceivedBytes;
                lastSentSegments = currentSentSegments;
                lastSentBytes = currentSentBytes;

                Thread.Sleep(1000);
            }
        }

        internal void TestSend(NetSession session)
        {
            using (var msgAck = MessagePool.Rent())
            {
                msgAck.ID = (MessageID)0x2001;
                msgAck.TryWrite(this.Name);
                msgAck.TryWrite(byte.MinValue);
                msgAck.Size = 4090;

                session.Send(msgAck);
            }
        }
    }

    internal class SampleServerNetworkConfig : INetServerConfig
    {
        public int Backlog => 100;

        public int SocketCount => 1024;

        public int TaskCount => Environment.ProcessorCount;

        public INetSessionFactory NetSessionFactory => SampleServerNetSession.Factory;
    }

    internal class SampleServerNetSession : NetServerSession
    {
        #region Factory

        private class SampleServerNetSessionFactory : INetSessionFactory
        {
            public NetSession Create(NetEngine engine, int id) => new SampleServerNetSession(engine, id);
        }

        public static INetSessionFactory Factory { get; } = new SampleServerNetSessionFactory();

        #endregion Factory

        public SampleServerNetSession(NetEngine engine, int id) : base(engine, id)
        {
        }
    }

    internal class SampleServerNet : NetEngineServer
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly SampleServerService _service;

        private readonly SampleServerHandler _sampleHandler;

        public SampleServerNet(SampleServerService service, INetServerConfig config) : base(config)
        {
            _service = service;

            _sampleHandler = new SampleServerHandler(_service, this.MessageManager);
        }

        protected override void OnAccepted(NetSession session)
        {
            base.OnAccepted(session);
            Logger.Info($"Accepted: {session.ID}");
        }

        protected override void OnDisconnected(NetSession session, NetDisconnectReason reason)
        {
            base.OnDisconnected(session, reason);
            Logger.Info($"Disconnected: {session.ID}: {reason}");
        }
    }

    internal class SampleServerHandler : MessageHandler
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly SampleServerService _service;

        public SampleServerHandler(SampleServerService service, MessageManager manager) : base(manager)
        {
            _service = service;

            manager.RegisterHandler(MessageID.Create(MessageDirection.NoDir, MessageType.Framework, 0x0001), this.SampleHandler1);
        }

        private MessageResult SampleHandler1(NetSession session, Message msg)
        {
            if (!msg.TryRead(out string remoteService))
                return MessageResult.Error;

            //Logger.Info(remoteService);

            _service.TestSend(session);
            return MessageResult.Success;
        }
    }
}